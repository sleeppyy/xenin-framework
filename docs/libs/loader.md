# Loader <shared>Shared</shared>
`XeninUI.Loader` utilises the [Builder Pattern](https://en.wikipedia.org/wiki/Builder_pattern), which means all operations returns the object, allowing method chaining as seen in the example below.
```laux
XeninUI.Loader()
  :setName("My Addon")
  :setAcronym("Addon")
  :setDirectory("my_addon_path")
  :setColor(XeninUI.Theme.Orange)
:done()
```

# Enumerations
* **XENINUI_SERVER** = 1
* **XENINUI_CLIENT** = 2
* **XENINUI_SHARED** = 3

# Methods
All write operations to build the object is available here.

## setName
**Required.** Used to set the name of the addon.
```laux
:setName(name: string)
```
**Arguements**
1. What name the addon should have.

---

## setAcronym
**Required.** Used to set the acronym of the addon.
```laux
:setAcronym(acronym: string)
```
**Arguements**
1. What acronym the addon should have.

---

## setDirectory
**Required.** Used to set the root directory of the addon.
```laux
:setDirectory(directory: string)
```
**Arguements**
1. What's the addon's root directory.

---

## setColor
**Required.** Used to set the color of the addon, that will be used into console logging.
```laux
:setColor(color: Color)
```
**Arguements**
1. What color the addon should have.

---

## loadMessage
Used internally to print debug messages
```laux
:loadMessage(path: string, realm: string, col = self:getColor())
```
**Arguements**
1. File path
2. Prefix's realm
3. **Optional** Prefix's color

---

## loadFile
Also used internally to load a single file
```laux
:loadFile(path: string, realm: number, func: function)
```
**Arguements**
1. File path
2. Realm (You can use [enumerations](/libs/loader?id=enumerations))  
3. **Optional** Load function

---

## load
Used to load a folder (can be loaded recursively)
```laux
:load(dir: string, realm: number|table, recursive = false: boolean, options = {})
```
**Arguements**
1. Directory
2. Realm (You can use [enumerations](/libs/loader?id=enumerations))
3. **Optional** Should load folder recursively
4. **Optional** Options (`ignoreFiles = {}`, `overwriteRealm = {}`)
**Options**
1. ignoreFiles
```laux
ignoreFiles = {
    [FILE_NAME] = true,
}
```
2. overwriteRealm
```laux
overwriteRealm = {
    [FILE_NAME] = REALM,
}
```

---

## done
Finishes the action currently in the works.
```laux
:done()
```

# Example
This example come from the Xenin Framework (`laux/autorun/xeninui_load.laux`).
```laux
XeninUI.Loader()
	:setName("Xenin Framework")
	:setAcronym("Xenin")
	:setDirectory("xeninui")
	:setColor(XeninUI.Theme.Red)
	:load("libs", XENINUI_CLIENT, false, {
		ignoreFiles = {
			loader = true
		},
		overwriteRealm = {
			essentials_sh = XENINUI_SHARED,
			v0n_sh = XENINUI_SHARED,
			promises = XENINUI_SHARED,
			permissions = XENINUI_SHARED
		}
	})
	:load("libs/network", XENINUI_SHARED)
	:load("server", XENINUI_SERVER, true)
	:load("libs/languages", XENINUI_SHARED)
	:load("libs/languages/network", {
		client = XENINUI_CLIENT,
		server = XENINUI_SERVER
	})
	:load("libs/scripts", XENINUI_SHARED)
	:load("libs/scripts/network", {
		client = XENINUI_CLIENT,
		server = XENINUI_SERVER
	})
	:load("libs/configurator", XENINUI_SHARED)
	:load("libs/configurator/classes", XENINUI_SHARED, false, {
		overwriteRealm = {
			database = XENINUI_SERVER
		}
	})
	:load("libs/configurator/network", {
		client = XENINUI_CLIENT,
		server = XENINUI_SERVER
	})
	:load("libs/configurator/ui", XENINUI_CLIENT, true)
	:load("libs/config", XENINUI_SHARED)
	:load("libs/config/network", {
		client = XENINUI_CLIENT,
		server = XENINUI_SERVER
	})
	:load("elements", XENINUI_CLIENT)
	:load("core/ui", XENINUI_CLIENT)
	:load("libs/units", XENINUI_SHARED, true)
:done()
```